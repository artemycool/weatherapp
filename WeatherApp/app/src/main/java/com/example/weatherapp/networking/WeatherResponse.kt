package com.example.weatherapp.networking

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class WeatherResponse<T>(
    val data: List<T>,
    val success: Boolean,
    var error: String?
)