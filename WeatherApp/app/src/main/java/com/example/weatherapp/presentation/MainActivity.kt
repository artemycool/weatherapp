package com.example.weatherapp.presentation

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.cityapp.R
import com.example.cityapp.databinding.ActivityMainBinding
import com.example.weatherapp.data.model.City
import com.example.weatherapp.utill.ConnectionLiveData
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.viewmodel.ext.android.viewModel


open class MainActivity : AppCompatActivity() {

    private lateinit var spinnerCitiesAdapter: ArrayAdapter<City>
    val weatherViewModel: WeatherViewModel by viewModel()
    private lateinit var connectionLiveData: ConnectionLiveData
    private lateinit var binding: ActivityMainBinding
    private var dayWeatherAdapter = DayWeatherAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.weather_toolbar))
        initTopBarMenu()
        initRecyclerView()
        setUpSubscriptions()
        binding.viewmodel = weatherViewModel
        binding.lifecycleOwner = this
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.add_city -> {
            showAddingCityAlert()
            true
        }

        R.id.delete_current -> {
            weatherViewModel.deleteSelectedCity()
            dayWeatherAdapter.notifyDataSetChanged()
            true
        }

        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.weather_menu, menu)
        return true
    }

    /**
     * Initialize top bar menu
     */
    private fun initTopBarMenu() {

        spinnerCitiesAdapter = ArrayAdapter<City>(
            this, R.layout.city_spinner,
            mutableListOf()
        )

        spinnerCitiesAdapter.setDropDownViewResource(R.layout.city_spinner_item)
        binding.citySelectionSpinner.adapter = spinnerCitiesAdapter
        spinnerCitiesAdapter.notifyDataSetChanged()

        binding.citySelectionSpinner.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    pos: Int,
                    id: Long
                ) {
                    val city = city_selection_spinner.selectedItem as? City
                    weatherViewModel.selectCity(city)
                }

                override fun onNothingSelected(arg0: AdapterView<*>?) {}
            }
    }

    private fun initRecyclerView() {
        binding.dailyRv.adapter = dayWeatherAdapter
        binding.dailyRv.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
    }

    private fun setUpSubscriptions() {
        connectionLiveData = ConnectionLiveData(this)
        weatherViewModel.allCities.observe(this, { cities ->
            spinnerCitiesAdapter.clear()
            spinnerCitiesAdapter.addAll(cities)
            spinnerCitiesAdapter.notifyDataSetChanged()
        })
        weatherViewModel.dailyCities.observe(this, { dailyWeatherList ->
            dayWeatherAdapter.data = dailyWeatherList
        })

        connectionLiveData.observe(this) {
            weatherViewModel.setInternetConnection(it)
        }

        weatherViewModel.messageForUser.observe(this) { message ->
            Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
        }

        weatherViewModel.selectedCity.observe(this) { city ->
            weatherViewModel.allCities.observe(this) { cities ->
                val position = cities.indexOfFirst { it.cityName == city.cityName }
                if (position >= 0) {
                    binding.citySelectionSpinner.setSelection(position)
                }
            }
        }


    }

    private fun showAddingCityAlert() {
        AddCityDialogFragment.newInstance().show(getSupportFragmentManager(), "AddCityDialog")
    }

}