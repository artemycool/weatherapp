package com.example.weatherapp.presentation

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import androidx.fragment.app.DialogFragment
import com.example.cityapp.databinding.AddCityDialogBinding
import com.example.weatherapp.data.model.City
import org.koin.android.viewmodel.ext.android.viewModel


class AddCityDialogFragment : DialogFragment() {

    private val weatherViewModel: WeatherViewModel by viewModel()
    private var binding: AddCityDialogBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dialog?.window?.apply {
            setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            requestFeature(Window.FEATURE_NO_TITLE)
        }
        binding = AddCityDialogBinding.inflate(inflater, container, false)
        return binding?.getRoot()
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT
        )
        binding?.addCityBtn?.setOnClickListener {
            weatherViewModel.addCity(City(cityName = binding?.addCityEdt?.text.toString()))
            dismiss()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    companion object {
        @JvmStatic
        fun newInstance() = AddCityDialogFragment()
    }


}

