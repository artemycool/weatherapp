package com.example.weatherapp.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CurrentCityWeather(
    @Json(name = "temp")
    val temperature: String,
    @Json(name = "feels_like")
    var feelsLike: String = "",
    @Json(name = "weather")
    var currentWeatherDescription: List<CurrentWeatherDescription> = mutableListOf(
        CurrentWeatherDescription(
            "No data", "4d"
        )
    )
) {
    companion object {
        val defaultCurrentCityWeather = CurrentCityWeather(
            temperature = "No data",
            feelsLike = "No data"
        )
    }
}

@JsonClass(generateAdapter = true)
data class CurrentWeatherDescription(
    @Json(name = "main")
    val weatherDescription: String = "",
    @Json(name = "icon")
    val icon: String = "",
)