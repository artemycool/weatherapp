package com.example.weatherapp.presentation

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.cityapp.R
import com.example.cityapp.databinding.DailyWeatherItemBinding
import com.example.weatherapp.data.model.DailyWeather
import com.example.weatherapp.utill.toDayOfWeek
import com.squareup.picasso.Picasso

class DayWeatherAdapter(private val context: Context) :
    RecyclerView.Adapter<DayWeatherAdapter.ViewHolder>() {

    var data = listOf<DailyWeather>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItemCount() = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<DailyWeatherItemBinding>(
            inflater,
            R.layout.daily_weather_item,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]
        holder.bind(item, context)
    }

    class ViewHolder(private var binding: DailyWeatherItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: DailyWeather, context: Context) {
            binding.apply {
                currentDay.text = item.time.toDayOfWeek()
                currentWeather.text = context.getString(R.string.weather_format, item.temperature.day)
                currentWeatherDesc.text = item.weatherDescription.get(0).weatherDescription
                val url = context.getString(
                    R.string.load_image_url_format,
                    item.weatherDescription.get(0).icon
                )
                Picasso.get().load(url)
                    .resize(imageSize, imageSize).centerCrop()
                    .into(currentWeatherImage)
            }
        }

        companion object {
            private const val imageSize = 50
        }
    }
}
