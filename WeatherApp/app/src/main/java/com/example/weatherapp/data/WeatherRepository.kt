package com.example.weatherapp.data

import com.example.weatherapp.data.database.CityDao
import com.example.weatherapp.data.model.City
import com.example.weatherapp.data.model.GeographicCoordinates
import com.example.weatherapp.data.model.toCity
import com.example.weatherapp.data.model.toEntity
import com.example.weatherapp.networking.CityInfoResponse
import com.example.weatherapp.networking.WeatherApiEndPoints
import io.reactivex.rxjava3.schedulers.Schedulers
import io.reactivex.rxjava3.subjects.BehaviorSubject
import io.reactivex.rxjava3.subjects.PublishSubject
import org.koin.core.KoinComponent
import timber.log.Timber

class WeatherRepository(private val weatherApi: WeatherApiEndPoints, private val cityDao: CityDao) :
    KoinComponent {

    val cityInfoResponse: PublishSubject<CityInfoResponse<Any>> = PublishSubject.create()
    val cities: PublishSubject<List<City>> = PublishSubject.create()
    val selectedCity: PublishSubject<City> = PublishSubject.create()
    val isConnectedToInternet: BehaviorSubject<Boolean> = BehaviorSubject.create()

    fun getCityWeather(city: City) {
        Timber.d("Getting weather for city:$city")
        cityInfoResponse.onNext(CityInfoResponse.Loading())
        weatherApi.getCityGeographicCoordinates(city.cityName)
            .subscribeOn(Schedulers.io())
            .doOnError {
                getCityWeatherFromDataBase(city)
            }
            .subscribeOn(Schedulers.io())
            .subscribe { apiResponse ->
                when (apiResponse.code()) {
                    200 -> {
                        val body = apiResponse.body()
                        if (body?.isNotEmpty() == true) {
                            // Get the first city coordinates
                            val data = body[0]
                            getWeatherByCoordinates(data, city)
                        } else {
                            getCityWeatherFromDataBase(city)
                        }
                    }
                    else -> {
                        getCityWeatherFromDataBase(city)
                    }
                }
            }
    }


    private fun getWeatherByCoordinates(geographicCoordinates: GeographicCoordinates, city: City) {
        val latitude = geographicCoordinates.latitude
        val longitude = geographicCoordinates.longitude
        if (latitude != null && longitude != null) {
            weatherApi.getCityByGeographicCoordinates(
                latitude,
                longitude
            )
                .subscribeOn(Schedulers.io())
                .doOnError {
                    Timber.d("Something goes wrong during getting city weather by coordinates")
                }.subscribe { apiResponse ->
                    when (apiResponse.code()) {
                        200 -> {
                            Timber.d("Result of getting weather by coordinated ${apiResponse.code()}")
                            val weatherData = apiResponse.body()
                            if (weatherData == null) {
                                getCityWeatherFromDataBase(city)
                                return@subscribe
                            }
                            val updatedCity = weatherData.copy(cityName = city.cityName)
                            cityDao.insertAll(updatedCity.toEntity())
                            cityInfoResponse.onNext(CityInfoResponse.Success(updatedCity))
                        }
                        else -> {
                           getCityWeatherFromDataBase(city)
                        }
                    }
                }
        }
    }

    fun setInternetConnection(isConnected: Boolean) {
        isConnectedToInternet.onNext(isConnected)
    }

    fun getCityWeatherFromDataBase(city: City?) {
        if (city == null) {
            Timber.d("There are no selected city")
            return
        }
        Timber.d("Getting weather from database")
        val cityInfo = cityDao.loadCityByName(cityName = city.cityName)?.toCity()
        if (cityInfo != null) {
            cityInfoResponse.onNext(CityInfoResponse.Success(cityInfo))
        } else {
            cityInfoResponse.onNext(CityInfoResponse.Error())
        }
    }

    fun loadCities() {
        cityDao.getAll().subscribe { newCities ->
            cities.onNext(newCities.map { it.toCity() })
        }
    }

    fun addCity(city: City) {
        getCityWeather(city)
    }

    fun deleteCity(city: City) {
        cityDao.deleteByCityName(city.cityName)
    }
}