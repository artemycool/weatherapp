package com.example.weatherapp.di


import com.example.weatherapp.data.database.CityDatabase
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val databaseModule = module {
    single { CityDatabase.getInstance(androidApplication()) }
    single { get<CityDatabase>().cityDao }
}