package com.example.weatherapp.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.weatherapp.data.model.CityEntity
import io.reactivex.rxjava3.core.Observable

@Dao
interface CityDao {
    @Query("SELECT * FROM cities")
    fun getAll(): Observable<List<CityEntity>>

    @Query("SELECT * FROM cities WHERE cityName IN (:cityName)")
    fun loadCityByName(cityName: String): CityEntity?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg cities: CityEntity)

    @Query("DELETE FROM cities WHERE cityName = :cityName")
    fun deleteByCityName(cityName: String)
}
