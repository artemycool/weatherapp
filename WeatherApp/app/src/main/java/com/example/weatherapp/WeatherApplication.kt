package com.example.cityapp

import android.app.Application
import com.example.weatherapp.di.databaseModule
import com.example.weatherapp.di.forecastModule
import com.example.weatherapp.di.networkModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.logger.AndroidLogger
import org.koin.core.context.startKoin

class CityApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        // Start Koin
        startKoin {
            AndroidLogger()
            androidContext(this@CityApplication)
            modules(listOf(networkModule, databaseModule,forecastModule))
        }
    }
}