package com.example.cityapp.networking

import okhttp3.Interceptor
import okhttp3.Response

class AuthInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var req = chain.request()
        val url =
            req.url.newBuilder().addQueryParameter("appid", "db8c340f57f0e66fefabc9ab2574e66f")
                .build()
        req = req.newBuilder().url(url).build()
        return chain.proceed(req)
    }
}