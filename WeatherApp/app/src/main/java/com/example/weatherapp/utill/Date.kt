package com.example.weatherapp.utill

import java.text.SimpleDateFormat
import java.util.*

/**
 * Convenient method to get current date.
 * You can modify pattern for you needs.
 */
fun getDate(): String {
    val dateFormat = SimpleDateFormat("MM-dd-yyyy")
    return dateFormat.format(Calendar.getInstance().time)
}

/**
 * Convert unix time to day of week
 */
fun Long.toDayOfWeek(): String {
    val date = Date(this * 1000)
    val dateFormat = SimpleDateFormat("EEEE", Locale.ENGLISH)
    return dateFormat.format(date).toString()
}