package com.example.weatherapp.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class GeographicCoordinates(
    @Json(name = "lat")
    val latitude: Double?,
    @Json(name = "lon")
    val longitude: Double?,
)