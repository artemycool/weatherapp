package com.example.weatherapp.data.model

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.cityapp.R
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import com.squareup.picasso.Picasso


@JsonClass(generateAdapter = true)
data class City(
    @Transient
    var cityName: String = "",

    @Json(name = "current")
    var currentCityWeather: CurrentCityWeather = CurrentCityWeather.defaultCurrentCityWeather,

    @Json(name = "daily")
    var dailyCityWeather: List<DailyWeather>? = null,
) {
    override fun toString(): String {
        return cityName
    }
}

@JsonClass(generateAdapter = true)
data class DailyWeather(
    @Json(name = "dt")
    val time: Long,
    @Json(name = "temp")
    val temperature: Temperature,
    @Json(name = "weather")
    var weatherDescription: List<CurrentWeatherDescription> = mutableListOf(
        CurrentWeatherDescription(
            "No data", "4d"
        )
    )
)

@JsonClass(generateAdapter = true)
data class Temperature(
    val day: String = ""
)

fun City.toEntity(): CityEntity {
    return CityEntity(
        cityName = this.cityName,
        currentWeather = this.currentCityWeather.temperature,
        feelsLike = this.currentCityWeather.feelsLike,
        icon = this.currentCityWeather.currentWeatherDescription[0].icon,
        description = this.currentCityWeather.currentWeatherDescription[0].weatherDescription
    )
}

fun CityEntity.toCity(): City {
    return City(
        cityName = this.cityName,
        currentCityWeather = CurrentCityWeather(
            temperature = this.currentWeather,
            feelsLike = this.feelsLike,
            currentWeatherDescription = mutableListOf(
                CurrentWeatherDescription(
                    weatherDescription = this.description,
                    icon = this.icon
                )
            )
        )
    )
}

@Entity(tableName = "cities")
data class CityEntity(
    @PrimaryKey
    var cityName: String,
    var currentWeather: String = "",
    var feelsLike: String = "",
    var icon: String = "10d",
    var description: String = ""
)


@BindingAdapter("android:url_image")
fun imageLoader(imageView: ImageView, urlFormat: String?) {
    val url = imageView.context.getString(R.string.load_image_url_format, urlFormat)
    Picasso.get()
        .load(url)
        .resize(100, 100)
        .centerCrop()
        .into(imageView)
}