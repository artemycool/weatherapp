package com.example.weatherapp.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.weatherapp.data.model.CityEntity

@Database(entities = [CityEntity::class], version = 1)
abstract class CityDatabase : RoomDatabase() {
    abstract val cityDao: CityDao

    companion object {
        @Volatile
        private var INSTANCE: CityDatabase? = null

        fun getInstance(context: Context): CityDatabase? {
            if (INSTANCE == null) {
                INSTANCE = provideDataBase(context)
            }
            return INSTANCE
        }

        private fun provideDataBase(context: Context): CityDatabase {
            synchronized(CityDatabase::class) {
                return Room.databaseBuilder(
                    context.applicationContext,
                    CityDatabase::class.java,
                    "City database"
                )
                    .addCallback(object : RoomDatabase.Callback() {
                        override fun onCreate(db: SupportSQLiteDatabase) {
                            super.onCreate(db)
                            Thread {
                                prepopulateData.forEach {
                                    getInstance(context)?.cityDao?.insertAll(it)
                                }
                            }.start()
                        }
                    })
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build()
            }
        }

        val prepopulateData = arrayListOf(

            CityEntity(
                cityName = "Moscow",
                currentWeather = "24",
                icon = "10d",
                description = "Clear",
                feelsLike = "25"
            ),
            CityEntity(
                cityName = "Saint Petersburg",
                currentWeather = "24",
                icon = "10d",
                description = "Clear",
                feelsLike = "25"
            )
        )
    }
}

