package com.example.weatherapp.networking

import com.example.weatherapp.data.model.City
import com.example.weatherapp.data.model.GeographicCoordinates
import io.reactivex.rxjava3.core.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApiEndPoints {
    @GET("data/2.5/onecall?&exclude=hourly&units=metric")
    fun getCityByGeographicCoordinates(
        @Query("lat") latitude: Double, @Query("lon") longitude: Double,
    ): Single<Response<City>>

    @GET("geo/1.0/direct?&limit=1")
    fun getCityGeographicCoordinates(
        @Query("q") cityName: String
    ): Single<Response<List<GeographicCoordinates>>>
}