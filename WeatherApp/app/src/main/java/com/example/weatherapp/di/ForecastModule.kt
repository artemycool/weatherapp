package com.example.weatherapp.di


import com.example.weatherapp.data.WeatherRepository
import com.example.weatherapp.presentation.WeatherViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val forecastModule = module {
    single { WeatherRepository(get(),get()) }
    viewModel { WeatherViewModel(get(),get()) }
}