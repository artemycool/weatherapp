package com.example.weatherapp.presentation

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.cityapp.R
import com.example.weatherapp.data.WeatherRepository
import com.example.weatherapp.data.model.City
import com.example.weatherapp.data.model.DailyWeather
import com.example.weatherapp.networking.CityInfoResponse
import com.example.weatherapp.utill.getDate
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.Observables
import timber.log.Timber


class WeatherViewModel(
    private val context: Application,
    private val weatherRepository: WeatherRepository
) : AndroidViewModel(context) {

    val allCities: MutableLiveData<List<City>> = MutableLiveData<List<City>>()
    val messageForUser: MutableLiveData<String> = MutableLiveData()
    val currentTemperature = MutableLiveData("")
    val feelsLikeTemperature = MutableLiveData("")
    val currentDayInfo = MutableLiveData<String>()
    val currentWeatherDescription = MutableLiveData("")
    val currentImageUrl = MutableLiveData<String>()
    val dailyCities = MutableLiveData<List<DailyWeather>>()
    val selectedCity: MutableLiveData<City> = MutableLiveData()

    private val compositeDisposable = CompositeDisposable()

    init {
        setUpSubscriptions()
        loadCities()
        currentDayInfo.postValue(getDate())
    }

    private fun setUpSubscriptions() {
        compositeDisposable.addAll(
            Observables.combineLatest(
                weatherRepository.selectedCity,
                weatherRepository.isConnectedToInternet,
            )
                .doOnError {
                    Timber.e("Error during weather loading :$it")
                }
                .subscribe { pair ->
                    val city = pair.first
                    val isConnected = pair.second
                    loadCityInfo(city, isConnected)
                },

            weatherRepository.cities.subscribe {
                allCities.postValue(it.sortedBy { it.cityName })
            },

            weatherRepository.cityInfoResponse.doOnError {
                Timber.e("Error during getting weather=$it")
            }.subscribe { response ->
                when (response) {
                    is CityInfoResponse.Success -> {
                        response.data?.let { city ->
                            if (city is City) {
                                updateUiData(city)
                            }
                        }
                    }
                    is CityInfoResponse.Error -> {
                        messageForUser.postValue(context.getString(R.string.city_not_found_error))
                    }
                    is CityInfoResponse.Loading -> {
                        Timber.d("Weather is loading")
                    }
                }
            })
    }

    private fun loadCityInfo(city: City, isConnected: Boolean) {
        if (isConnected) {
            weatherRepository.getCityWeather(city)
        } else {
            messageForUser.value = context.getString(R.string.network_connection_error)
            weatherRepository.getCityWeatherFromDataBase(city)
        }
    }

    private fun updateUiData(city: City) {
        val dailyCitiesWeather = city.dailyCityWeather ?: emptyList()
        dailyCities.postValue(dailyCitiesWeather)
        selectedCity.postValue(city)
        currentTemperature.postValue(city.currentCityWeather.temperature)
        feelsLikeTemperature.postValue(city.currentCityWeather.feelsLike)
        currentImageUrl.postValue(city.currentCityWeather.currentWeatherDescription[0].icon)
        currentWeatherDescription.postValue(city.currentCityWeather.currentWeatherDescription[0].weatherDescription)
    }

    fun setInternetConnection(isConnected: Boolean) {
        weatherRepository.setInternetConnection(isConnected)
    }

    private fun loadCities() {
        weatherRepository.loadCities()
    }

    fun selectCity(city: City?) {
        city?.let {
            weatherRepository.selectedCity.onNext(city)
        } ?: kotlin.run {
            Timber.e("Error in city selection logic")
        }
    }

    fun addCity(city: City) {
        weatherRepository.isConnectedToInternet.firstElement().subscribe { isConnected ->
            if (isConnected) {
                weatherRepository.addCity(city)
            } else {
                messageForUser.value = context.getString(R.string.network_connection_error)
            }
        }
    }

    fun deleteSelectedCity() {
        val selectedCity = selectedCity.value
        if (selectedCity == null) {
            Timber.d("Selected city is not selected or there are no selected city")
            return
        }
        weatherRepository.deleteCity(selectedCity)
    }

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }
}