package com.example.weatherapp.networking

/**
 * Class to handle api and database requests about city weather
 */
sealed class CityInfoResponse<T>(
    val data: T? = null,
    val success: Boolean = false,
    val message: String? = null
) {

    class Success<T>(data: T) : CityInfoResponse<T>(data = data)

    class Error<T>(message: String? = "", data: T? = null) :
        CityInfoResponse<T>(data = data, message = message)

    class Loading<T> : CityInfoResponse<T>()

}